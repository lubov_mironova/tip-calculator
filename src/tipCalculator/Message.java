package tipCalculator;

public interface Message {
    String INPUT_BILL_AMOUNT = "Введите сумму чека";
    String EMPTY_FIELD = "Пустое поле";
    String NEGATIVE_NUMBER = "Введите сумму чека больше нуля";
    String BILL_AMOUNT_IN_RUBLES = "Сумма чека в рублях: ";
    String TIP_TO_PAY_IN_RUBLES = "Сумма чаевых в рублях: ";
    String TOTAL_PAYABLE_IN_RUBLES = "Всего к оплате в рублях: ";
    double DOLLAR_RATE = 72.65;
    double EURO_RATE = 79.21;
}