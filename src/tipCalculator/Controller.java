package tipCalculator;

import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * Расчет чаевых
 *
 * @author Mironova L.
 */
public class Controller implements Message {

    @FXML
    private TextField checkAmountField;

    @FXML
    private RadioButton leaveFivePercentTipRadioButton;

    @FXML
    private ToggleGroup tipGroup;

    @FXML
    private RadioButton leaveTenPercentTipRadioButton;

    @FXML
    private Label errorLabel;

    @FXML
    private ComboBox<String> choiceCurrencyComboBox;

    @FXML
    private TextField tipToPayField;

    @FXML
    private Label tipToPayInRublesLabel;

    @FXML
    private CheckBox doRoundCheckBox;

    @FXML
    private CheckBox printInRublesCheckBox;

    @FXML
    private TextField totalPayableField;

    @FXML
    private Label totalPayableInRublesLabel;

    /**
     * Очищает метки
     */
    private void clearLabel(Label label) {
        if (!label.getText().isEmpty()) label.setText("");
    }

    /**
     * Возвращает порядковый номер выбранной валюты
     *
     * @return порядковый номер выбранной валюты
     */
    @FXML
    private int selectedCurrency() {
        setDisable();
        switch (choiceCurrencyComboBox.getValue()) {
            case "$":
                return 2;
            case "€":
                return 3;
        }
        return 1;
    }

    /**
     * Делает недоступным чекбокс "вывести в рублях" если выбрана валюта рубли
     */
    private void setDisable() {
        printInRublesCheckBox.setDisable(choiceCurrencyComboBox.getValue().equals("₽"));
    }

    /**
     * Проверяет текстовое поле на наличие введенных данных
     *
     * @return false, если поле пустое и true, если в поле не пустое
     */
    private boolean isAmount() {
        if (checkAmountField.getText().isEmpty()) {
            changeLabelColorToRed();
            errorLabel.setText(EMPTY_FIELD);
            return false;
        }
        return true;
    }

    /**
     * Сохраняет введенное в текстовое поле значение суммы чека
     *
     * @return введенное значение суммы чека или null, если введены нечисловые данные
     */
    private Double giveNumber() {
        try {
            double amount = Double.parseDouble(checkAmountField.getText().replace(",", "."));
            if (amount <= 0) {
                throw new Exception("Negative number");
            }
            return amount;
        } catch (NumberFormatException e) {
            changeLabelColorToRed();
            errorLabel.setText(INPUT_BILL_AMOUNT);
        } catch (Exception e) {
            changeLabelColorToRed();
            errorLabel.setText(NEGATIVE_NUMBER);
        }
        return null;
    }

    /**
     * Возвращается порядковый номер выбранной радиокнопки
     *
     * @return число - порядковый номер радиокнопки
     */
    private int returnChoiceRadioButton() {
        Toggle choice = tipGroup.getSelectedToggle();
        if (choice == leaveFivePercentTipRadioButton) {
            return 1;
        }
        if (choice == leaveTenPercentTipRadioButton) {
            return 2;
        }
        return 3;
    }

    /**
     * Считает чаевые в зависимости от выбранного процента чаевых
     *
     * @param amount сумма чека
     * @return чаевые в зависимости от выбранного процента чаевых
     */
    private double tipCount(double amount) {
        switch (returnChoiceRadioButton()) {
            case 1:
                amount = amount * 0.05;
                break;
            case 2:
                amount = amount * 0.1;
                break;
            case 3:
                amount = 0;
        }
        return amount;
    }

    /**
     * Метод форматированного вывода вещественного числа с двумя знаками после запятой
     *
     * @param number число для форматированного вывода
     * @return форматированное до двух знаков число
     */
    private Double numberFormation(Double number) {
        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Проверяет, выбран ли чекбокс
     *
     * @param checkBox чекбокс для проверки
     * @return true если выбран, false если не выбран
     */
    @FXML
    private boolean isSelected(CheckBox checkBox) {
        return checkBox.isSelected();
    }

    /**
     * Вызывает метод округления числа до целого, если выбран чекбокс
     *
     * @param aDouble число, которое необходимо округлить до целого
     * @return округленное до целого число, если выбран чекбокс или неокругленное число, если не выбран чекбокс
     */
    private double doRound(double aDouble) {
        if (isSelected(doRoundCheckBox)) {
            if (returnChoiceRadioButton() == 3) {
                if (aDouble % 1 != 0) {
                    aDouble += 0.5;
                }
            }
            return makeRound(aDouble);
        }
        return aDouble;
    }

    /**
     * Округляет число до целого
     *
     * @param number число, которое необходимо округлить до целого
     * @return округленное до целого число
     */
    private double makeRound(double number) {
        return new BigDecimal(number).setScale(0, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Очищает поля, метки и делает чекбоксы не выбранными
     */
    @FXML
    private void clear() {
        checkAmountField.clear();
        totalPayableField.clear();
        tipToPayField.clear();
        clearRublesLabel();
        doRoundCheckBox.setSelected(false);
        printInRublesCheckBox.setSelected(false);
    }

    /**
     * Меняет цвет метки на красный
     */
    void changeLabelColorToRed() {
        errorLabel.setStyle("-fx-text-fill: red");
    }

    /**
     * Меняет цвет метки на черный
     */
    void changeLabelColorToBlack() {
        errorLabel.setStyle("-fx-text-fill: black");
    }

    /**
     * Рассчитывает и выводит результат
     */
    @FXML
    private void giveResult() {
        if (isAmount() && giveNumber() != null) {
            double amount = giveNumber();

            clearLabel(errorLabel);
            double tip = tipCount(amount);
            switch (selectedCurrency()) {
                case 1:
                    clearRublesLabel();
                    computesResultForField(amount, tip, " ₽");
                    break;
                case 2:
                    changeLabelColorToBlack();
                    computesResultForField(amount, tip, "$ ");
                    setResultOrClearLabel(amount, tip);
                    break;
                case 3:
                    changeLabelColorToBlack();
                    computesResultForField(amount, tip, "€ ");
                    setResultOrClearLabel(amount, tip);
            }
        }
    }

    /**
     * Выводит результат в текстовое поле с двумя знаками после запятой
     *
     * @param number    число для вывода
     * @param textField текстовое поле, в которое необходимо вывести число
     * @param symbol    строка с символом валюты
     */
    private void printResultInTextField(Double number, TextField textField, String symbol) {
        DecimalFormat twoDForm = new DecimalFormat("#0.00");
        if (symbol.equals(" ₽")) {
            textField.setText(twoDForm.format(number).replace(",", ".") + symbol);
        } else {
            textField.setText(symbol + twoDForm.format(number).replace(",", "."));
        }
    }

    /**
     * Выводит результат в метку с двумя знаками после запятой
     *
     * @param number   число для вывода
     * @param label    метка, в которую необходимо вывести число
     * @param constant заданная константой строка
     */
    private void printResultInLabel(Double number, Label label, String constant) {
        DecimalFormat twoDForm = new DecimalFormat("#0.00");
        label.setText(constant + twoDForm.format(number).replace(",", "."));
    }

    /**
     * Вызывает метод для вывода результата в метки, иначе очищает метки
     *
     * @param amount   сумма чека
     * @param tip      чаевые
     */
    private void setResultOrClearLabel(double amount, double tip) {
        if (isSelected(printInRublesCheckBox)) {
            computesResultForLabel(amount, tip);
        } else {
            clearRublesLabel();
        }
    }

    /**
     * Вычисляет результат для меток
     *
     * @param amount сумма чека
     * @param tip    чаевые
     */
    private void computesResultForLabel(double amount, double tip) {
        String select = choiceCurrencyComboBox.getValue();
        HashMap<String, Double> currency = new HashMap<>();
        currency.put("$", DOLLAR_RATE);
        currency.put("€", EURO_RATE);

        for (String key : currency.keySet()) {
            if (select.equals(key)) {
                double value = currency.get(key);
                printResultInLabel(numberFormation(amount * value), errorLabel, BILL_AMOUNT_IN_RUBLES);
                printResultInLabel(numberFormation(doRound((amount + tip) * value)), totalPayableInRublesLabel, TOTAL_PAYABLE_IN_RUBLES);
                printResultInLabel(numberFormation(tip * value), tipToPayInRublesLabel, TIP_TO_PAY_IN_RUBLES);
            }
        }
    }

    /**
     * Очищает метки, в которые выводятся чаевые, сумма чека и общая сумма в рублях
     */
    private void clearRublesLabel() {
        clearLabel(tipToPayInRublesLabel);
        clearLabel(totalPayableInRublesLabel);
        clearLabel(errorLabel);
    }

    /**
     * Вычисляет результат для текстовых полей
     *
     * @param amount   сумма чека
     * @param tip      чаевые
     * @param currency строка с символом валют
     */
    private void computesResultForField(double amount, double tip, String currency) {
        printResultInTextField(numberFormation(doRound(amount + tip)), totalPayableField, currency);
        printResultInTextField(numberFormation(tip), tipToPayField, currency);
    }
}